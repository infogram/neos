// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "eos_base.h"

#pragma pack(push, 8)

#undef EOS_RESULT_VALUE
#undef EOS_RESULT_VALUE_LAST
#define EOS_RESULT_VALUE(Name, Value) Name = Value,
#define EOS_RESULT_VALUE_LAST(Name, Value) Name = Value

EOS_ENUM_START(EOS_EResult)
#include "eos_result.h"
EOS_ENUM_END(EOS_EResult);

#undef EOS_RESULT_VALUE
#undef EOS_RESULT_VALUE_LAST

/**
 * Returns a string representation of an EOS_EResult. 
 * The return value is never null.
 * The return value must not be freed.
 *
 * Example: EOS_EResult_ToString(EOS_Success) returns "EOS_Success"
 */
EOS_DECLARE_FUNC(const char*) EOS_EResult_ToString(EOS_EResult Result);

/**
 * Returns whether a result is to be considered the final result, or false if the callback that returned this result
 * will be called again either after some time or from another action.
 *
 * @param Result The result to check against being a final result for an operation
 * @return True if this result means the operation is complete, false otherwise
 */
EOS_DECLARE_FUNC(EOS_Bool) EOS_EResult_IsOperationComplete(EOS_EResult Result);

/**
 * Encode a byte array into hex encoded string
 *
 * @return An EOS_EResult that indicates whether the byte array was converted and copied into the OutBuffer.
 *         EOS_Success if the encoding was successful and passed out in OutBuffer
 *         EOS_InvalidParameters if you pass a null pointer on invalid length for any of the parameters
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the encoding. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_ByteArray_ToString(const uint8_t* ByteArray, const uint32_t Length, char* OutBuffer, uint32_t* InOutBufferLength);

/** A handle to a user's Epic account ID (store related account ecosystem) */
typedef struct EOS_EpicAccountIdDetails* EOS_EpicAccountId;

/** 
 * Check whether or not the given account unique id is considered valid
 * 
 * @param AccountId The account id to check for validity
 * @return EOS_TRUE if the EOS_EpicAccountId is valid, otherwise EOS_FALSE
 */
EOS_DECLARE_FUNC(EOS_Bool) EOS_EpicAccountId_IsValid(EOS_EpicAccountId AccountId);

/**
 * Retrieve a string-ified account ID from an EOS_EpicAccountId. This is useful for replication of Epic account IDs in multiplayer games.
 *
 * @param AccountId The account ID for which to retrieve the string-ified version.
 * @param OutBuffer The buffer into which the character data should be written
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the account ID string was copied into the OutBuffer.
 *         EOS_Success - The OutBuffer was filled, and InOutBufferLength contains the number of characters copied into OutBuffer excluding the null terminator.
 *         EOS_InvalidParameters - Either OutBuffer or InOutBufferLength were passed as NULL parameters.
 *         EOS_InvalidUser - The AccountId is invalid and cannot be string-ified
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the account ID string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_EpicAccountId_ToString(EOS_EpicAccountId AccountId, char* OutBuffer, int32_t* InOutBufferLength);

/**
 * Retrieve an EOS_EpicAccountId from a raw account ID string. The input string must be null-terminated.
 *
 * @param AccountIdString The string-ified account ID for which to retrieve the EOS_EpicAccountId
 * @return The EOS_EpicAccountId that corresponds to the AccountIdString
 */
EOS_DECLARE_FUNC(EOS_EpicAccountId) EOS_EpicAccountId_FromString(const char* AccountIdString);

/** A character buffer of this size is large enough to fit a successful output of EOS_EpicAccountId_ToString */
#define EOS_EPICACCOUNTID_MAX_LENGTH 128

/** A handle to a user's Epic account ID (game service related account ecosystem) */
typedef struct EOS_ProductUserIdDetails* EOS_ProductUserId;

/**
 * Check whether or not the given account unique id is considered valid
 *
 * @param AccountId The account id to check for validity
 * @return EOS_TRUE if the EOS_ProductUserId is valid, otherwise EOS_FALSE
 */
EOS_DECLARE_FUNC(EOS_Bool) EOS_ProductUserId_IsValid(EOS_ProductUserId AccountId);

/**
 * Retrieve a string-ified account ID from an EOS_ProductUserId. This is useful for replication of Product User IDs in multiplayer games.
 *
 * @param AccountId The account ID for which to retrieve the string-ified version.
 * @param OutBuffer The buffer into which the character data should be written
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the account ID string was copied into the OutBuffer.
 *         EOS_Success - The OutBuffer was filled, and InOutBufferLength contains the number of characters copied into OutBuffer excluding the null terminator.
 *         EOS_InvalidParameters - Either OutBuffer or InOutBufferLength were passed as NULL parameters.
 *         EOS_InvalidUser - The AccountId is invalid and cannot be string-ified
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the account ID string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_ProductUserId_ToString(EOS_ProductUserId AccountId, char* OutBuffer, int32_t* InOutBufferLength);

/**
 * Retrieve an EOS_EpicAccountId from a raw account ID string. The input string must be null-terminated.
 *
 * @param AccountIdString The string-ified account ID for which to retrieve the EOS_ProductUserId
 * @return The EOS_ProductUserId that corresponds to the AccountIdString
 */
EOS_DECLARE_FUNC(EOS_ProductUserId) EOS_ProductUserId_FromString(const char* AccountIdString);

/** A character buffer of this size is large enough to fit a successful output of EOS_ProductUserId_ToString */
#define EOS_PRODUCTUSERID_MAX_LENGTH 128

/** An invalid EOS_NotificationID */
#define EOS_INVALID_NOTIFICATIONID ((EOS_NotificationId)0)

/** Handle to an existing registered notification (0 is an invalid handle) */
EXTERN_C typedef uint64_t EOS_NotificationId;

/** A handle to a continuance token @see eos_connect.h */
typedef struct EOS_ContinuanceTokenDetails* EOS_ContinuanceToken;

/** The most recent version of the EOS_DeviceInfo struct. */
#define EOS_AUTH_DEVICEINFO_API_LATEST 1

EOS_STRUCT(EOS_DeviceInfo, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Type of the device */
	const char* Type;
	/** Model of the device */
	const char* Model;
	/** OS of the device */
	const char* OS;
));

/** The most recent version of the EOS_PageQuery and EOS_PageResult structs. */
#define EOS_PAGINATION_API_LATEST 1

/** The default MaxCount used for a EOS_PageQuery when the API allows the EOS_PageQuery to be omitted.
 */
#define EOS_PAGEQUERY_MAXCOUNT_DEFAULT 10

/** The maximum MaxCount used for a EOS_PageQuery.
 */
#define EOS_PAGEQUERY_MAXCOUNT_MAXIMUM 100

/**
 * A page query is part of query options. It is used to allow pagination of query results.
 */
EOS_STRUCT(EOS_PageQuery, (
	/** Version of the API */
	int32_t ApiVersion;
	/** The index into the ordered query results to start the page at. */
	int32_t StartIndex;
	/** The maximum number of results to have in the page. */
	int32_t MaxCount;
));

/**
 * A page result is part of query callback info. It is used to provide pagination details of query results.
 */
EOS_STRUCT(EOS_PageResult, (
	/** The index into the ordered query results to start the page at. */
	int32_t StartIndex;
	/** The number of results in the current page. */
	int32_t Count;
	/** The number of results associated with they original query options. */
	int32_t TotalCount;
));

/**
 * All possible states of a local user
 *
 * @see EOS_Auth_AddNotifyLoginStatusChanged
 * @see EOS_Auth_GetLoginStatus
 * @see EOS_Auth_Login
 * @see EOS_Connect_AddNotifyLoginStatusChanged
 * @see EOS_Connect_GetLoginStatus
 * @see EOS_Connect_Login
 */
EOS_ENUM(EOS_ELoginStatus,
	/** Player has not logged in or chosen a local profile */
	EOS_LS_NotLoggedIn = 0,
	/** Player is using a local profile but is not logged in */
	EOS_LS_UsingLocalProfile = 1,
	/** Player has been validated by the platform specific authentication service */
	EOS_LS_LoggedIn = 2
);

#pragma pack(pop)
