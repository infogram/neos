#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Returns whether a result is to be considered the final result, or false if the callback that returned this result
 * will be called again either after some time or from another action.
 *
 * @param Result The result to check against being a final result for an operation
 * @return True if this result means the operation is complete, false otherwise
 */
EOS_DECLARE_FUNC(EOS_Bool) EOS_EResult_IsOperationComplete(EOS_EResult Result)
{
  log(LL::Debug, __FUNCTION__ " (%d)", Result);

  PROXY_FUNC(EOS_EResult_IsOperationComplete);
  if (proxied)
  {
    auto res = proxied(Result);
    log(LL::Debug, " + Proxy Result: %d", res);
    return res;
  }

  // TODO!
  return true;
}

/**
 * Encode a byte array into hex encoded string
 *
 * @return An EOS_EResult that indicates whether the byte array was converted and copied into the OutBuffer.
 *         EOS_Success if the encoding was successful and passed out in OutBuffer
 *         EOS_InvalidParameters if you pass a null pointer on invalid length for any of the parameters
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the encoding. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_ByteArray_ToString(const uint8_t* ByteArray, const uint32_t Length, char* OutBuffer, uint32_t* InOutBufferLength)
{
  if(InOutBufferLength)
    log(LL::Debug, __FUNCTION__ " (%p, %d, %p, %p(%d))", ByteArray, Length, OutBuffer, InOutBufferLength, *InOutBufferLength);
  else
    log(LL::Debug, __FUNCTION__ " (%p, %d, %p, %p)", ByteArray, Length, OutBuffer, InOutBufferLength);

  PROXY_FUNC(EOS_ByteArray_ToString);
  if (proxied)
  {
    auto res = proxied(ByteArray, Length, OutBuffer, InOutBufferLength);
    log(LL::Debug, " + Proxy Result: %d", res);
    return res;
  }

  if (!ByteArray || !Length || !InOutBufferLength)
    return EOS_EResult::EOS_InvalidParameters;

  uint32_t input_size = *InOutBufferLength;
  *InOutBufferLength = Length * 2;
  if (!OutBuffer)
    return EOS_EResult::EOS_InvalidParameters;

  if (input_size < *InOutBufferLength)
    return EOS_EResult::EOS_LimitExceeded;

  for (uint32_t i = 0; i < Length; i++)
    sprintf_s(OutBuffer + i * 2, 3, "%02x", ByteArray[i]);

  return EOS_EResult::EOS_Success;
}
