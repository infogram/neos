#include "../stdafx.h"
#include "../NEOS.hpp"

EOS_AllocateMemoryFunc GameAllocMemoryFunc = nullptr;

EOS_InitializeOptions CustomInitializeOptions;
EOS_Platform_Options CustomPlatformOptions;

/**
 * Initialize the Epic Online Services SDK.
 *
 * Before calling any other function in the SDK, clients must call this function.
 *
 * This function must only be called one time and must have a corresponding EOS_Shutdown call.
 *
 * @param Options - The initialization options to use for the SDK.
 * @return An EOS_EResult is returned to indicate success or an error.
 *
 * EOS_Success is returned if the SDK successfully initializes.
 * EOS_AlreadyConfigured is returned if the function has already been called.
 * EOS_InvalidParameters is returned if the provided options are invalid.
 */

EOS_DECLARE_FUNC(EOS_EResult) EOS_Initialize(const EOS_InitializeOptions* Options)
{
	if (Options)
	{
		CustomInitializeOptions = *Options;

		log(LL::Info, "EOS_Initialize (%p(%d, %p, %p, %p, %s, %s))", Options, Options->ApiVersion, Options->AllocateMemoryFunction, Options->ReallocateMemoryFunction, Options->ReleaseMemoryFunction, Options->ProductName, Options->ProductVersion);

		EOS_CHECK_VERSION(EOS_INITIALIZE_API_LATEST);

		// Save the games memory allocation func for use in DLC queries...
		if (Options->AllocateMemoryFunction)
			GameAllocMemoryFunc = Options->AllocateMemoryFunction;
		else
		{
			log(LL::Warning, " - Game didn't provide memory-allocation function! Forced DLC likely won't work!");
			if (!ForcedDLCUseMalloc)
				log(LL::Warning, " - (Maybe try using ForcedDLC->UseMalloc in the ini!)");
		}

		if (Options->ProductName)
		{
			log(LL::Info, " - Product Name: %s", Options->ProductName);
			GameProductName = Options->ProductName;
		}
		if (Options->ProductVersion)
		{
			log(LL::Info, " - Product Version: %s", Options->ProductVersion);
			GameProductVersion = Options->ProductVersion;
		}

		// Overrides
		if (!OverrideProductName.empty())
		{
			log(LL::Info, " - Overriding with Product Name: %s", OverrideProductName.c_str());
			CustomInitializeOptions.ProductName = OverrideProductName.c_str();
			Options = &CustomInitializeOptions;
		}
		if (!OverrideProductVersion.empty())
		{
			log(LL::Info, " - Overriding with Product Version: %s", OverrideProductVersion.c_str());
			CustomInitializeOptions.ProductVersion = OverrideProductVersion.c_str();
			Options = &CustomInitializeOptions;
		}
	}
	else
		log(LL::Info, "EOS_Initialize (%p)", Options);

	if (!EOSIsInited)
		NEOS_Main();

	EOS_EResult res = EOS_EResult::EOS_Success;

	PROXY_FUNC(EOS_Initialize);
	if (proxied)
	{
		res = proxied(Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);

		// If proxy init is successful, setup log callbacks from EOS
		if (res == EOS_EResult::EOS_Success && ProxyHookLogs)
			NEOS_InitProxyLogging();

		//return res; // we want to init too...
	}

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	if (EOSIsInited)
		return EOS_EResult::EOS_AlreadyConfigured;

	EOSIsInited = true;
	EOSIsShutdown = false; // should we do this?

	return res;
}

/**
 * Tear down the Epic Online Services SDK.
 *
 * Once this function has been called, no more SDK calls are permitted; calling anything after EOS_Shutdown will result in undefined behavior.
 * @return An EOS_EResult is returned to indicate success or an error.
 * EOS_Success is returned if the SDK is successfully torn down.
 * EOS_NotConfigured is returned if a successful call to EOS_Initialize has not been made.
 * EOS_UnexpectedError is returned if EOS_Shutdown has already been called.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Shutdown()
{
	log(LL::Debug, "EOS_Shutdown");

	EOS_EResult res = EOS_EResult::EOS_Success;

	PROXY_FUNC(EOS_Shutdown);
	if (proxied)
	{
		res = proxied();
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		// return res; // we want to shutdown too...
	}

	if (!EOSIsInited)
		return EOS_EResult::EOS_NotConfigured;

	if (EOSIsShutdown)
		return EOS_EResult::EOS_UnexpectedError;

	EOSIsInited = false;
	EOSIsShutdown = true;

	return res;
}

/**
 * Create a single Epic Online Services Platform Instance.
 *
 * The platform instance is used to gain access to the various Epic Online Services.
 *
 * This function returns an opaque handle to the platform instance, and that handle must be passed to EOS_Platform_Release to release the instance.
 *
 * @return An opaque handle to the platform instance.
 */
EOS_DECLARE_FUNC(EOS_HPlatform) EOS_Platform_Create(const EOS_Platform_Options* Options)
{
	if (Options)
	{
		CustomPlatformOptions = *Options;

		log(LL::Debug, "EOS_Platform_Create (%p(%d, %p, %s, %s, (%s, %s), %d)", Options, Options->ApiVersion, Options->Reserved, Options->ProductId, Options->SandboxId,
			Options->ClientCredentials.ClientId, Options->ClientCredentials.ClientSecret, Options->bIsServer);

		EOS_CHECK_VERSION(EOS_PLATFORM_OPTIONS_API_LATEST);
		
		if (Options->ProductId)
		{
			log(LL::Info, " - Product Id: %s", Options->ProductId);
			GameProductId = Options->ProductId;
		}
		if (Options->SandboxId)
		{
			log(LL::Info, " - Sandbox Id: %s", Options->SandboxId);
			GameSandboxId = Options->SandboxId;
		}
		if (Options->ClientCredentials.ClientId)
		{
			log(LL::Info, " - Client Id: %s", Options->ClientCredentials.ClientId);
			GameClientId = Options->ClientCredentials.ClientId;
		}
		if (Options->ClientCredentials.ClientSecret)
		{
			log(LL::Info, " - Client Secret: %s", Options->ClientCredentials.ClientSecret);
			GameClientSecret = Options->ClientCredentials.ClientSecret;
		}

		if (Options->ApiVersion >= 5)
		{
			if (Options->EncryptionKey)
				log(LL::Info, " - File Encryption Key: %s", Options->EncryptionKey);
			if (Options->OverrideCountryCode)
				log(LL::Info, " - Override Country Code (provided by game): %s", Options->OverrideCountryCode);
			if (Options->OverrideLocaleCode)
				log(LL::Info, " - Override Locale Code (provided by game): %s", Options->OverrideLocaleCode);
			if (Options->DeploymentId)
				log(LL::Info, " - Deployment Id: %s", Options->DeploymentId);

			if (Options->ApiVersion >= 6)
			{
				if (Options->CacheDirectory)
					log(LL::Info, " - Cache Directory: %s", Options->CacheDirectory);
			}

			log(LL::Info, " - Flags: %lld", Options->Flags);
			if (Options->Flags & EOS_PF_LOADING_IN_EDITOR)
				log(LL::Debug, " - (EOS_PF_LOADING_IN_EDITOR)");
			if (Options->Flags & EOS_PF_DISABLE_OVERLAY)
				log(LL::Debug, " - (EOS_PF_DISABLE_OVERLAY)");
		}

		// Overrides
		if (!OverrideProductId.empty())
		{
			log(LL::Info, " - Overriding with Product Id: %s", OverrideProductId.c_str());
			CustomPlatformOptions.ProductId = OverrideProductId.c_str();
			Options = &CustomPlatformOptions;
		}
		if (!OverrideSandboxId.empty())
		{
			log(LL::Info, " - Overriding with Sandbox Id: %s", OverrideSandboxId.c_str());
			CustomPlatformOptions.SandboxId = OverrideSandboxId.c_str();
			Options = &CustomPlatformOptions;
		}
		if (!OverrideClientId.empty())
		{
			log(LL::Info, " - Overriding with Client Id: %s", OverrideClientId.c_str());
			CustomPlatformOptions.ClientCredentials.ClientId = OverrideClientId.c_str();
			Options = &CustomPlatformOptions;
		}
		if (!OverrideClientSecret.empty())
		{
			log(LL::Info, " - Overriding with Client Secret: %s", OverrideClientSecret.c_str());
			CustomPlatformOptions.ClientCredentials.ClientSecret = OverrideClientSecret.c_str();
			Options = &CustomPlatformOptions;
		}
		if (Options->ApiVersion >= EOS_PLATFORM_OPTIONS_API_LATEST)
		{
			if (!OverrideEncryptionKey.empty())
			{
				log(LL::Info, " - Overriding with Encryption Key: %s", OverrideEncryptionKey.c_str());
				CustomPlatformOptions.EncryptionKey = OverrideEncryptionKey.c_str();
				Options = &CustomPlatformOptions;
			}
			if (!OverrideDeploymentId.empty())
			{
				log(LL::Info, " - Overriding with Deployment Id: %s", OverrideDeploymentId.c_str());
				CustomPlatformOptions.DeploymentId = OverrideDeploymentId.c_str();
				Options = &CustomPlatformOptions;
			}
		}
	}
	else
		log(LL::Debug, "EOS_Platform_Create (%p)", Options);

	auto* platform = new HPlatform();
	
	if (Options)
	{
		platform->m_ApiVersion = Options->ApiVersion;
		platform->m_Reserved = Options->Reserved;
		if(Options->ProductId)
			platform->m_ProductId = Options->ProductId;
		if(Options->SandboxId)
			platform->m_SandboxId = Options->SandboxId;
		if(Options->ClientCredentials.ClientId)
			platform->m_ClientCredentials.ClientId = Options->ClientCredentials.ClientId;
		if(Options->ClientCredentials.ClientSecret)
			platform->m_ClientCredentials.ClientSecret = Options->ClientCredentials.ClientSecret;
		platform->m_bIsServer = Options->bIsServer;
		if (Options->ApiVersion >= EOS_PLATFORM_OPTIONS_API_LATEST)
		{
			if (Options->EncryptionKey)
				platform->m_EncryptionKey = Options->EncryptionKey;
			if (Options->OverrideCountryCode)
				platform->m_OverrideCountryCode = Options->OverrideCountryCode;
			if (Options->OverrideLocaleCode)
				platform->m_OverrideLocaleCode = Options->OverrideLocaleCode;
			if (Options->DeploymentId)
				platform->m_DeploymentId = Options->DeploymentId;
			platform->m_Flags = Options->Flags;
		}
	}

	PROXY_FUNC(EOS_Platform_Create);
	if (proxied)
	{
		auto res = proxied(Options);
		log(LL::Debug, " + Proxy Result: %p", res);
		platform->SetProxyHandle(res);
	}

	return reinterpret_cast<EOS_HPlatform>(platform);
}

/**
 * Release an Epic Online Services platform instance previously returned from EOS_Platform_Create.
 *
 * This function should only be called once per instance returned by EOS_Platform_Create. Undefined behavior will result in calling it with a single instance more than once.
 * Typically only a single platform instance needs to be created during the lifetime of a game.
 * You should release each platform instance before calling the EOS_Shutdown function.
 */
EOS_DECLARE_FUNC(void) EOS_Platform_Release(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_Release (%p)", Handle);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_Release);
	if (proxied)
		proxied(platform->GetProxyHandle());

	delete platform;
}
