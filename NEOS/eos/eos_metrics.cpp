#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Logs the start of a new game session for a local player.
 *
 * The game client should call this function whenever it joins into a new multiplayer, peer-to-peer or single player game session.
 * Each call to BeginPlayerSession must be matched with a corresponding call to EndPlayerSession.
 *
 * @param Options Structure containing the local player's game account and the game session information.
 *
 * @return Returns EOS_Success on success, or an error code if the input parameters are invalid or an active session for the player already exists.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Metrics_BeginPlayerSession(EOS_HMetrics Handle, const EOS_Metrics_BeginPlayerSessionOptions* Options)
{
	log(LL::Debug, "EOS_Metrics_BeginPlayerSession (%p)", Options);

	EOS_CHECK_VERSION(EOS_METRICS_BEGINPLAYERSESSION_API_LATEST);

	PROXY_FUNC(EOS_Metrics_BeginPlayerSession);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_Success;
}

/**
 * Logs the end of a game session for a local player.
 *
 * Call once when the game client leaves the active game session.
 * Each call to BeginPlayerSession must be matched with a corresponding call to EndPlayerSession.
 *
 * @param Options Structure containing the account id of the player whose session to end.
 *
 * @return Returns EOS_Success on success, or an error code if the input parameters are invalid or there was no active session for the player.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Metrics_EndPlayerSession(EOS_HMetrics Handle, const EOS_Metrics_EndPlayerSessionOptions* Options)
{
	log(LL::Debug, "EOS_Metrics_EndPlayerSession (%p)", Options);

	EOS_CHECK_VERSION(EOS_METRICS_ENDPLAYERSESSION_API_LATEST);

	PROXY_FUNC(EOS_Metrics_EndPlayerSession);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_Success;
}
