#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Reject an invite from another player.
 *
 * @param Options Structure containing information about the invite to reject
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the reject invite operation completes, either successfully or in error
 *
 * @return EOS_Success if the invite rejection completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_NotFound if the invite does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_RejectInvite(EOS_HSessions Handle, const EOS_Sessions_RejectInviteOptions* Options, void* ClientData, const EOS_Sessions_OnRejectInviteCallback CompletionDelegate)
{
  if (Options && Options->InviteId)
    log(LL::Debug, __FUNCTION__ " (%p(%d, %p(%s), %p), %p, %p)", Options, Options->ApiVersion, Options->InviteId, Options->InviteId, Options->LocalUserId, ClientData, CompletionDelegate);
  else
    log(LL::Debug, __FUNCTION__ " (%p, %p, %p)", Options, ClientData, CompletionDelegate);

  EOS_CHECK_VERSION(EOS_SESSIONS_REJECTINVITE_API_LATEST);

  PROXY_FUNC(EOS_Sessions_RejectInvite);
  if (proxied)
  {
    proxied(Handle, Options, ClientData, CompletionDelegate);
    return;
  }

  //currently unused:
  //EOS_Sessions_RejectInviteOptions options;
  //if (Options)
  //	options = *Options;

  NEOS_AddCallback([Options, ClientData, CompletionDelegate]()
    {
      EOS_Sessions_RejectInviteCallbackInfo cbi;
      cbi.ClientData = ClientData;
      cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
      CompletionDelegate(&cbi);
    });
}

/**
 * EOS_Sessions_IsUserInSession returns whether or not a given user can be found in a specified session
 *
 * @param Options Structure containing the input parameters
 *
 * @return EOS_Success if the user is found in the specified session
 *		   EOS_NotFound if the user is not found in the specified session
 *		   EOS_InvalidParameters if you pass an invalid invite id or a null pointer for the out parameter
 *		   EOS_IncompatibleVersion if the API version passed in is incorrect
 *		   EOS_Invalid_ProductUserID if an invalid target user is specified
 *		   EOS_Sessions_InvalidSession if the session specified is invalid
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_IsUserInSession(EOS_HSessions Handle, const EOS_Sessions_IsUserInSessionOptions* Options)
{
  if (Options)
    log(LL::Debug, __FUNCTION__ " (%p(%d, %p(%s), %p))", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, Options->TargetUserId);
  else
    log(LL::Debug, __FUNCTION__ " (%p)", Options);

  EOS_CHECK_VERSION(EOS_SESSIONS_ISUSERINSESSION_API_LATEST);

  PROXY_FUNC(EOS_Sessions_IsUserInSession);
  if (proxied)
  {
    auto res = proxied(Handle, Options);
    log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
    return res;
  }

  EOS_CHECK_CONFIGURED();

  // TODOSTUB
  return EOS_EResult::EOS_InvalidParameters;
}

/**
 * EOS_ActiveSession_GetRegisteredPlayerByIndex is used to immediately retrieve individual players registered with the active session.
 *
 * @param Options Structure containing the input parameters
 *
 * @return the product user id for the registered player at a given index or null if that index is invalid
 *
 * @see EOS_ActiveSession_GetRegisteredPlayerCount
 * @see EOS_ActiveSession_GetRegisteredPlayerByIndexOptions
 */
EOS_DECLARE_FUNC(EOS_ProductUserId) EOS_ActiveSession_GetRegisteredPlayerByIndex(EOS_HActiveSession Handle, const EOS_ActiveSession_GetRegisteredPlayerByIndexOptions* Options)
{
  if (Options)
    log(LL::Debug, __FUNCTION__ " (%p(%d, %d))", Options, Options->ApiVersion, Options->PlayerIndex);
  else
    log(LL::Debug, __FUNCTION__ " (%p)", Options);

  EOS_CHECK_VERSION(EOS_ACTIVESESSION_GETREGISTEREDPLAYERBYINDEX_API_LATEST);

  PROXY_FUNC(EOS_ActiveSession_GetRegisteredPlayerByIndex);
  if (proxied)
  {
    auto res = proxied(Handle, Options);
    log(LL::Debug, " + Proxy Result: %p", res);
    return res;
  }

  // TODOSTUB
  return 0;
}

/**
 * EOS_SessionDetails_CopySessionAttributeByKey is used to immediately retrieve a copy of session attribution from a given source such as a active session or a search result.
 * If the call returns an EOS_Success result, the out parameter, OutSessionAttribute, must be passed to EOS_SessionDetails_Attribute_Release to release the memory associated with it.
 *
 * @param Options Structure containing the input parameters
 * @param OutSessionAttribute Out parameter used to receive the EOS_SessionDetails_Attribute structure.
 *
 * @return EOS_Success if the information is available and passed out in OutSessionAttribute
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_SessionDetails_Info
 * @see EOS_SessionDetails_CopySessionAttributeByKeyOptions
 * @see EOS_SessionDetails_Attribute_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionDetails_CopySessionAttributeByKey(EOS_HSessionDetails Handle, const EOS_SessionDetails_CopySessionAttributeByKeyOptions* Options, EOS_SessionDetails_Attribute** OutSessionAttribute)
{
  if (Options)
    log(LL::Debug, __FUNCTION__ " (%p(%d, %p(%s)))", Options, Options->ApiVersion, Options->AttrKey, Options->AttrKey);
  else
    log(LL::Debug, __FUNCTION__ " (%p)", Options);

  EOS_CHECK_VERSION(EOS_SESSIONDETAILS_COPYSESSIONATTRIBUTEBYKEY_API_LATEST);

  PROXY_FUNC(EOS_SessionDetails_CopySessionAttributeByKey);
  if (proxied)
  {
    auto res = proxied(Handle, Options, OutSessionAttribute);
    log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
    return res;
  }

  EOS_CHECK_CONFIGURED();

  // TODOSTUB
  return EOS_EResult::EOS_InvalidParameters;
}
